import { combineReducers } from 'redux';

import postsReducer from './hnPostsReducer'
import themeReducer from './themeReducers';
import { reducer as formReducer } from 'redux-form'

export default combineReducers({
	postsReducer,
	themeReducer,
	formReducer
});
