import * as types from '../constants'
import { updateState } from '../utils'

const initialState = []

const setPosts = (state, action) => {
    const prevPosts = state.posts

    return updateState(state, {
        posts: { ...prevPosts, ...action.payload },
        pending:  true
    })
}
const addPost = (state, action) => {
    const posts = state.posts
    posts.push(action.payload)

    return updateState(state, { posts: { ...posts } })
}
const removePost = (state, action) => {
    const posts = [...state.posts]
    const idx = posts.findIndex(p => p._id === action.payload)
    if (idx > -1) {
        posts.splice(idx, 1)
    }
    return updateState(state, { pending: false, posts })
}

const postsPending = (state, action) => {
    return updateState(state, { pending: true })
}
const postsSuccess = (state, action) => {
    return updateState(state, { pending: false, posts: action.payload })
}
const postsError = (state, action) => {
    return updateState(state, { pending: false, error: action.error })
}

function reducer(state = initialState, action) {
    switch (action.type) {

        case types.FETCH_POSTS_PENDING: return postsPending(state, action)
        case types.FETCH_POSTS_SUCCESS: return postsSuccess(state, action)
        case types.FETCH_POSTS_ERROR: return postsError(state, action)

        case types.DELETE_POST_PENDING: return postsPending(state, action)
        case types.DELETE_POST_SUCCESS: return removePost(state, action)
        case types.DELETE_POST_ERROR: return postsError(state, action)

        default: return state
    }
}

export const getPostsError = state => state.error
export const getPostsPending = state => state.pending
export const getPosts = state=> state.posts

export default reducer