import {
    FETCH_POSTS_PENDING,
    FETCH_POSTS_SUCCESS,
    FETCH_POSTS_ERROR,
    DELETE_POST_ERROR,
    DELETE_POST_PENDING,
    DELETE_POST_SUCCESS
} from '../constants'

export function fetchPostsPending() {
    return {
        type: FETCH_POSTS_PENDING
    }
}

export function fetchPostsSuccess(posts) {
    return {
        type: FETCH_POSTS_SUCCESS,
        payload: posts
    }
}

export function fetchPostsError(error) {
    return {
        type: FETCH_POSTS_ERROR,
        error: error
    }
}

export function deletePostPending() {
    return {
        type: DELETE_POST_PENDING
    }
}

export function deletePostSuccess(id) {
    return {
        type: DELETE_POST_SUCCESS,
        payload: id
    }
}
export function deletePostError(error) {
    return {
        type: DELETE_POST_ERROR,
        payload: error
    }
}