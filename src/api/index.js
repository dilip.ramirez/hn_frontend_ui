import axios from 'axios'
import {
    fetchPostsPending,
    fetchPostsSuccess,
    fetchPostsError,
    deletePostPending,
    deletePostSuccess,
    deletePostError
} from 'redux/actions'

const apiUrl = process.env.REACT_APP_POSTS_API_URL ? process.env.REACT_APP_POSTS_API_URL : window._env_.REACT_APP_POSTS_API_URL

export function fetchPosts() {
    return dispatch => {
        dispatch(fetchPostsPending())
        axios.get(`${apiUrl}/post`)
            .then((response) => {
                dispatch(fetchPostsSuccess(response.data))
            })
            .catch(error => dispatch(fetchPostsError(error)))
    }
}

export function deletePost(id) {
    return dispatch => {
        dispatch(deletePostPending())
        axios.delete(`${apiUrl}/post/${id}`, {
            data: {
                id: id
            }
        })
            .then(response => {
                dispatch(deletePostSuccess(id))
            })
            .catch(error => dispatch(deletePostError(error)))
    }
}