import React from "react";
import styled from "styled-components";

import { CircularProgress, Typography } from "@material-ui/core";

const Root = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100%;
  flex-direction: column;
`;
const LoadingLabel = styled(Typography)`
  margin-top: 5vh;
`
function Loader({ label }) {
  return (
    <Root>
      <CircularProgress m={2} color="secondary" />
      { label && <LoadingLabel align="center">{label}</LoadingLabel>}
    </Root>
  );
}

export default Loader;
