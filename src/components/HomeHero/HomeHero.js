import React from 'react'
import styled from 'styled-components'
import {
    Grid,
    Typography,
} from '@material-ui/core'

const HeroGrid = styled(Grid)`
    background-color: ${props => props.theme.palette.primary.main};
    color: white;
    padding: 10vh 0;
    text-align: center;
    h1 {
        font-size: 4em;
    }
`

function HomeHero(props) {
    return (
        <HeroGrid container alignContent="center">
            <Grid item xs={12} >
                <Typography variant="h1">
                    HN Feed
                </Typography>
                <Typography variant="h6" component="p">
                    We <span role="img" aria-label="love">❤️</span> Hacker News!
                </Typography>
            </Grid>
        </HeroGrid>
    )
}

export default HomeHero