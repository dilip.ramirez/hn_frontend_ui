import enhance from './HomeHero.enhancer'
import HomeHero from './HomeHero'

export default enhance(HomeHero)