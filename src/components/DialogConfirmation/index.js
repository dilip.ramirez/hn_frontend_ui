import React from 'react'
import {
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    // DialogContentText,
    DialogActions
} from '@material-ui/core'

function DialogConfirmation ({ state, onSuccess, onError, onClose, title, description, submitLabel = "Continue", cancelLabel = "Cancel", submitBtnClass = null, cancelBtnClass = null}) {

    const handleSubmit = () => onSuccess().then(onClose).catch(onError)
    
    return (
        <>
            <Dialog
                open={state}
                onClose={onClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
                <DialogContent id="alert-dialog-description">
                    {description}
                </DialogContent>
                <DialogActions>
                    <Button
                        onClick={onClose}
                    >
                        {cancelLabel}
                    </Button>
                    <Button
                        onClick={handleSubmit}
                        color="primary"
                    >
                        {submitLabel}
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    )

}

export default DialogConfirmation