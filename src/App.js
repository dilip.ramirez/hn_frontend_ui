import React, { useEffect } from 'react'
import { useDispatch, connect } from "react-redux"
import { Helmet } from 'react-helmet'

import DateFnsUtils from "@date-io/date-fns"
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles"
import { MuiPickersUtilsProvider } from "@material-ui/pickers"
import { StylesProvider } from "@material-ui/styles"
import { ThemeProvider } from "styled-components"

import maTheme from "theme"
import Routes from 'routes/Routes'

import { fetchPosts } from 'api'
import { getPostsError } from 'redux/reducers/hnPostsReducer'

function App({ theme }) {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchPosts())
  }, [])
  return (
    <>
      <Helmet
        titleTemplate="%s | HN News"
        defaultTitle="HN News"
      />
      <StylesProvider injectFirst>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <MuiThemeProvider theme={maTheme[theme.currentTheme]}>
            <ThemeProvider theme={maTheme[theme.currentTheme]}>
              <Routes />
            </ThemeProvider>
          </MuiThemeProvider>
        </MuiPickersUtilsProvider>
      </StylesProvider>
    </>
  )
}

export default connect(store => ({ theme: store.themeReducer }))(App)
