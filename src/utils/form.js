/**
 * Returns error message if value does not exist, otherwise returns
 * undefined
 * @param {string} value - Email to validate
 * @returns {string|undefined} Required string if value is undefined
 * @example <caption>Required Field</caption>
 * <Field
 *   name="password"
 *   component={TextField}
 *   label="Password"
 *   type="password"
 *   validate={required}
 * />
 */
export function required(value) {
    return value ? undefined : 'Requerido'
}

/**
 * Returns error message if value is not a valid email, otherwise returns
 * undefined
 * @param {string} value - Email to validate
 * @returns {string|undefined} Required string if value is undefined
 * @example <caption>Basic</caption>
 * <Field
 *   name="email"
 *   component={TextField}
 *   label="Email"
 *   validate={validateEmail}
 * />
 */
export function validateEmail(value) {
    return value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? 'E-mail inválido'
        : undefined
}

export const maxLength = m => v => v && v.length > m ? `Value must be ${m} character or less ` : undefined

export const minLength = m => v => v && v.length < m ? `Value must be ${m} character or more ` : undefined

export const arrayLength = s => a => a && a.length === 3 ? undefined : `This collection must have ${3} children.`

export const noPlaceholders = (k, s) => a => a && a.length && a.reduce((a, c, i) => c[k] !== s, false) ? undefined : `Items must not be equal to the placeholder '${s}'`

export const count = (k, v, c) => a => a && a.length && a.reduce((a, c, i) => a + (c[k] === v), 0) === c ? undefined : c > 0 ? `Item collection must contain only ${c} ${k > 1 ? `${k}s` : k}` : `Item collection must not contain ${k > 1 ? `${k}s` : k}`

export const alphaNumeric = (v) => !/[A-Za-z0-9-_]/.test(v) ? 'Sólo se aceptan caracteres alphanuméricos (a-z), números (0-9), y guiones (- y _)' : undefined

export const accept = b => v => v === b ? undefined : 'Debes aceptar esta casilla para continuar.'

export const matchToOther = (o) => (v, allValues) => v === allValues[o] ? undefined : 'Ambos valores deben ser idénticos'

// export const validators = function () {
//     return {
//         /**
//          * Array of validator functions to run against fields in validatedForm.
//          * This is populated when validators() is called.
//          *
//          * @example
//          * const fields = {
//          *    login: validators().required().alphaNumeric(),
//          *    firstName: validators().required()
//          * }
//          */
//         validators: [],
//         /**
//          * Adds a required validator to field
//          *
//          * @param {string} conditionalFieldKey - name of another field in form, if provided then this field is conditionally required if conditionalFieldKey is truthy.
//          * @param {string} conditionalFieldValue - to be used with conditionalFieldKey if you want this to be required based on another field's value.
//          * @param {string} msg - custom error message
//          */
//         required(conditionalFieldKey, conditionalFieldValue, msg = 'Required') {
//             this.validators.push((value, values) => {
//                 if (conditionalFieldValue) {
//                     return (conditionalFieldKey && values[conditionalFieldKey] === conditionalFieldValue && !value || !conditionalFieldKey && !value ? msg : null)
//                 } else {
//                     return (conditionalFieldKey && values[conditionalFieldKey] && !value || !conditionalFieldKey && !value ? msg : null)
//                 }
//             })
//             return this
//         },
//         /**
//          * Adds a minimum character length validator to field
//          *
//          * @param {number} minLength - field must not be less than minLength
//          * @param {string} msg - custom error message
//          */
//         min(minLength, msg) {
//             const defaultError = { message: ({ glTrans }) => (glTrans('LengthMustBeAtLeast', { minLength })) }
//             const errorMessage = msg || defaultError
//             this.validators.push((value, values) => (value && value.length < minLength ? errorMessage : null))
//             return this
//         },
//         /**
//          * Adds a maximum character length validator to field
//          *
//          * @param {number} maxLength - field must not be greater than maxLength
//          * @param {string} msg - custom error message
//          */
//         max(maxLength = 254, msg) {
//             const defaultError = { message: ({ glTrans }) => (glTrans('LengthCannotExceed', { maxLength })) }
//             const errorMessage = msg || defaultError
//             this.validators.push((value, values) => (value && value.length > maxLength ? errorMessage : null))
//             return this
//         },
//         /**
//          * Adds an alphaNumeric validator to field.
//          * Runs a regex test, checking that value contains only letters & numbers with no spaces or special characters.
//          *
//          * @param {string} msg - custom error message
//          */
//         alphaNumeric(msg = 'ThisMustBeAlphanumeric') {
//             this.validators.push((value, values) => (value && !regex.alphaNumeric.test(value) ? msg : null))
//             return this
//         },
//         /**
//          * Adds a validator based on custom regular expression to field.
//          *
//          * @param {RegExp} pattern - regular expression to test field value
//          * @param {string} msg - custom error message
//          */
//         pattern(pattern, msg = 'InvalidPattern') {
//             this.validators.push((value, values) => (value && pattern.test(value) ? msg : null))
//             return this
//         },
//         /**
//          * Adds a validator that checks if this field is the same as another field
//          *
//          * @param {string} anotherField - name of another field in form that this this field must match.
//          * @param {string} msg - custom error message
//          */
//         matches(anotherField, msg = 'FieldsDoNotMatch') {
//             this.validators.push((value, values) => (value && value !== values[anotherField] ? msg : null))
//             return this
//         },
//         /**
//          * Adds an custom validator.
//          * Runs a function test, if predicate returns true then error is displayed, else no action
//          *
//          *  @param {string} msg - custom error message
//          */
//         custom(predicate, msg = 'FieldValueIncorrect') {
//             this.validators.push((value, values) => (value && predicate(value) ? msg : null))
//             return this
//         },
//         /**
//          * Adds a email validator to field
//          *
//          * @param {string} msg - custom error message
//          */
//         email(msg = 'InvalidEmailAddress') {
//             this.required(null, null, msg)
//             this.min(5)
//             this.max(255)
//             return this
//         },
//         /**
//          * Adds a login validator to field
//          * This validator is composed of required, alphaNumeric, max
//          *
//          */
//         login() {
//             this.required()
//             this.alphaNumeric()
//             this.max(100)
//             return this
//         },
//         /**
//          * Adds a password validator to field
//          * This validator is composed of required, min, max.
//          *
//          * @param {string} conditionalFieldKey - conditionalFieldKey to pass to required validator.
//          */
//         password(conditionalFieldKey) {
//             const minLength = 6
//             const maxLength = 254
//             const minError = { message: ({ glTrans }) => (glTrans('PasswordTooShort', { minLength })) }
//             const maxError = { message: ({ glTrans }) => (glTrans('PasswordTooLong', { maxLength })) }

//             this.required(conditionalFieldKey)
//             this.min(minLength, minError)
//             this.max(maxLength, maxError)
//             return this
//         },
//         /**
//          * Adds a confirm password validator to field
//          * This validator is composed of required & matches.
//          *
//          * @param {string} newPasswordField - name of the 'newPassword' field in form that this this field must match
//          * @param {string} conditionalFieldKey - conditionalFieldKey to pass to required validator.
//          */
//         confirmPassword(newPasswordField, conditionalFieldKey) {
//             this.required(conditionalFieldKey)
//             this.matches(newPasswordField, 'PasswordsDoNotMatch')
//             return this
//         }
//     }
// }