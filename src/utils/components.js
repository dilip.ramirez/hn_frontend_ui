import LoadableComponent from 'react-loadable'
import { branch, renderComponent } from 'recompose'
import Loader from '../components/Loader'

/**
 * Show a loading spinner when a condition is truthy. Used within
 * spinnerWhileLoading. Accepts a test function and a higher-order component.
 * @param {Function} condition - Condition function for when to show spinner
 * @returns {HigherOrderComponent}
 */
export function spinnerWhile(condition) {
  return branch(condition, renderComponent(Loader))
}

/**
 * HOC that shows a component while condition is true
 * @param {Function} condition - function which returns a boolean indicating
 * whether to render the provided component or not
 * @param {React.Component} component - React component to render if condition
 * is true
 * @returns {HigherOrderComponent}
 */
export function renderWhile(condition, component) {
  return branch(condition, renderComponent(component))
}

/**
 * Create component which is loaded async, showing a loading spinner
 * in the meantime.
 * @param {object} opts - Loading options
 * @param {Function} opts.loader - Loader function (should return import promise)
 * @returns {React.Component}
 */
export function Loadable(opts) {
  return LoadableComponent({
    loading: Loader,
    ...opts
  })
}