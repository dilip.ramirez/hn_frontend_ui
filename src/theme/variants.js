import { blue, green, grey, red} from "@material-ui/core/colors";

const lightVariant = {
  name: "Light",
  palette: {
    primary: {
      main: blue[800],
      contrastText: "#FFF"
    },
    secondary: {
      main: blue[600],
      contrastText: "#FFF"
    }
  },
  header: {
    color: grey[200],
    background: blue[800],
    search: {
      color: grey[100]
    },
    indicator: {
      background: red[700]
    }
  },
  filters: {
    toolbar: {
      list: {
        color: grey[500],
        active: {
          color: grey[800],
          background: grey[100]
        }
      }
    }
  },
  table: {
    hover: grey[50]
  },
  sidebar: {
    color: grey[900],
    background: "#FFF",
    header: {
      color: "#FFF",
      background: blue[800],
      brand: {
        color: "#FFFFFF"
      }
    },
    footer: {
      color: grey[900],
      background: grey[100],
      online: {
        background: green[500]
      }
    },
    category: {
      fontWeight: 600
    },
    badge: {
      color: "#FFF",
      background: green[600]
    }
  },
  body: {
    background: "#FFF"
  }
};

const darkVariant = {
  name: "Dark",
  palette: {
    primary: {
      main: blue[700],
      contrastText: "#FFF"
    },
    secondary: {
      main: blue[500],
      contrastText: "#FFF"
    }
  },
  header: {
    color: grey[500],
    background: "#FFFFFF",
    search: {
      color: grey[800]
    },
    indicator: {
      background: blue[600]
    }
  },
  filters: {
    toolbar: {
      list: {
        color: grey[500],
        active: {
          color: grey[800],
          background: grey[100]
        }
      }
    }
  },
  table: {
    hover: grey[50]
  },
  sidebar: {
    color: grey[200],
    background: "#1B2430",
    header: {
      color: grey[200],
      background: "#232f3e",
      brand: {
        color: blue[500]
      }
    },
    footer: {
      color: grey[200],
      background: "#232f3e",
      online: {
        background: green[500]
      }
    },
    category: {
      fontWeight: 400
    },
    badge: {
      color: "#FFF",
      background: blue[500]
    }
  },
  body: {
    background: "#FFF"
  }
};

const variants = [
  darkVariant,
  lightVariant,
];

export default variants;
