import enhance from './HomePage.enhancer'
import HomePage from './HomePage'

export default enhance(HomePage)