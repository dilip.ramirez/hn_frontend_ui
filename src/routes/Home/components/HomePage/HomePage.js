import React from 'react'
import styled from 'styled-components'
import {
    Grid
} from '@material-ui/core'

import HomeHero from 'components/HomeHero/HomeHero'
import PostsTable from '../PostsTable'

const HomePostsTableGrid = styled(Grid)`
    padding: 2vw;
`

function HomePage(props) {
    return (
        <Grid container>
            <Grid item xs={12}>
                <HomeHero />
            </Grid>
            <HomePostsTableGrid item xs={12}>
                <PostsTable />
            </HomePostsTableGrid>
        </Grid>
    )
}

export default HomePage