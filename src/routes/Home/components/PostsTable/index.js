import enhance from './PostsTable.enhancer'
import PostsTable from './PostsTable'

 export default enhance(PostsTable)