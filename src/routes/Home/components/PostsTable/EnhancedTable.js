import React from 'react'
import styled from "styled-components";
import { useSelector, useDispatch } from 'react-redux'

import moment from 'moment'

import {
    IconButton,
    Paper as MuiPaper,
    Table,
    TableBody,
    TableContainer,
    TableCell,
    TablePagination,
    TableRow,
    Typography,
    Snackbar
} from "@material-ui/core";

import {
    Close as CloseIcon,
} from "@material-ui/icons";

import { spacing } from "@material-ui/system";

import DialogConfirmation from 'components/DialogConfirmation'
import ActionMenu from './ActionMenu'

import { deletePost } from 'api'

/**
 * Note: Uncommenting lines will enable Table Header with sorting functionality that's out of the original scope of the project
 * but left in just in case.
 */

const Paper = styled(MuiPaper)(spacing);

const PostTableRow = styled(TableRow)`
    &:hover {
        backgrond-color: ${props => props.theme.table.hover};
        cursor: pointer;
    }
`
const AuthorTableCell = styled(TableCell)`
    color: #999;
`
const TitleDateTableCell = styled(TableCell)`
    color: #333;
    font-size: 13px;
`
function EnhancedTable() {
    const [order, setOrder] = React.useState('desc');
    const [orderBy, setOrderBy] = React.useState('created_at');
    const [selected, setSelected] = React.useState([]);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [dialogState, setDialogState] = React.useState(false)
    const [selectedRowTitle, setSelectedRowTitle] = React.useState(null)
    const [deleteSnackOpen, setDeleteSnackOpen] = React.useState(false)
    const [rowId, setRowId] = React.useState(null)

    const dispatch = useDispatch()

    let rows = useSelector(state => state.postsReducer.posts)
    rows = rows && rows.length ? rows : []
    
    const descendingComparator = (a, b, orderBy) => {
        if (b[orderBy] < a[orderBy]) {
            return -1;
        }
        if (b[orderBy] > a[orderBy]) {
            return 1;
        }
        return 0;
    }
    
    const getComparator = (order, orderBy) => {
        return order === 'desc'
            ? (a, b) => descendingComparator(a, b, orderBy)
            : (a, b) => -descendingComparator(a, b, orderBy);
    }
    
    function stableSort(array, comparator) {
        const stabilizedThis = array.map((el, index) => [el, index]);
        stabilizedThis.sort((a, b) => {
            const order = comparator(a[0], b[0]);
            if (order !== 0) return order;
            return a[1] - b[1];
        });
        return stabilizedThis.map((el) => el[0]);
    }

    const formatDate = date => {
        const ref = moment()
        const today = ref.clone().startOf('day')
        const yesterday = ref.clone().subtract(1, 'days').startOf('day')
        const mDate = moment(date)
        if (mDate.isSame(today, 'd')) {
            return mDate.format('HH:mm a')
        } else if (mDate.isSame(yesterday, 'd')) {
            return 'Yesterday'
        } else {
            return mDate.format('MMM DD')
        }
    }
    // const handleRequestSort = (event, property) => {
    //     const isAsc = orderBy === property && order === 'asc';
    //     setOrder(isAsc ? 'desc' : 'asc');
    //     setOrderBy(property);
    // };

    // const handleSelectAllClick = (event) => {
    //     if (event.target.checked) {
    //         const newSelecteds = rows.map((n) => n.name);
    //         setSelected(newSelecteds);
    //         return;
    //     }
    //     setSelected([]);
    // };

    // const handleClick = (event, name) => {
    //     const selectedIndex = selected.indexOf(name);
    //     let newSelected = [];

    //     if (selectedIndex === -1) {
    //         newSelected = newSelected.concat(selected, name);
    //     } else if (selectedIndex === 0) {
    //         newSelected = newSelected.concat(selected.slice(1));
    //     } else if (selectedIndex === selected.length - 1) {
    //         newSelected = newSelected.concat(selected.slice(0, -1));
    //     } else if (selectedIndex > 0) {
    //         newSelected = newSelected.concat(
    //             selected.slice(0, selectedIndex),
    //             selected.slice(selectedIndex + 1),
    //         );
    //     }

    //     setSelected(newSelected);
    // };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    // const handleRowUpdate = () => { }
    const handleRowClick = (row) => {
        const { story_url, url } = row
        let selectedUrl = undefined
        if (story_url) selectedUrl = story_url
        else selectedUrl = url

        if (selectedUrl) window.open(selectedUrl, '_blank')
    }

    const handleRemovalDialog = (id, title = null) => {
        setDialogState(true)
        setRowId(id)
        setSelectedRowTitle(title)
    }
    const handleRowRemoval = async (id) => {
        setDeleteSnackOpen(true)
        return dispatch(deletePost(id))
    }


    const handleMenuClose = () => undefined

    // const handleMenuOpen = (e) => {
    //     e.stopPropagation()
    //     // setAnchorEl(e.currentTarget)
    // }

    const handleSnackClose = () => setDeleteSnackOpen(false)

    const isSelected = (name) => selected.indexOf(name) !== -1;

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

    return (
        <div>
            <Paper elevation={0}>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right'
                    }}
                    open={deleteSnackOpen}
                    ContentProps={{
                        "aria-describedby": "message-id"
                    }}
                    autoHideDuration={5000}
                    message={<span id="message-id">Post deleted.</span>}
                    action={[
                        <IconButton
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            onClick={handleSnackClose}
                        >
                            <CloseIcon />
                        </IconButton>
                    ]}
                />
                <DialogConfirmation
                    id={rowId}
                    state={dialogState}
                    onSuccess={() => handleRowRemoval(rowId)}
                    onClose={(e) => setDialogState(false)}
                    onError={e => console.log(e)}
                    title={'Deleting Post'}
                    description={(
                        <>
                            <p>You are deleting the following post: </p>
                            <Typography variant="body1" component="p" align="center">"{selectedRowTitle}"</Typography>
                            <hr />
                            <p><strong>This action can't be undone.</strong></p>
                            <p>Continue?</p>
                        </>
                    )}
                />
                {/* <EnhancedTableToolbar numSelected={selected.length} /> */}
                <TableContainer>
                    <Table
                        aria-labelledby="tableTitle"
                        size={'small'}
                        aria-label="enhanced table"
                    >
                        {/* <EnhancedTableHead
                            numSelected={selected.length}
                            order={order}
                            orderBy={orderBy}
                            onSelectAllClick={handleSelectAllClick}
                            onRequestSort={handleRequestSort}
                            rowCount={rows.length}
                        /> */}
                        <TableBody>
                            {stableSort(rows, getComparator(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, index) => {
                                    const isItemSelected = isSelected(row.title);
                                    const labelId = `enhanced-table-checkbox-${index}`;

                                    return (
                                        <PostTableRow
                                            hover
                                            tabIndex={-1}
                                            key={row._id}
                                            onClick={() => handleRowClick(row)}
                                        >

                                            <TitleDateTableCell component="th" id={labelId} scope="row" padding="none">
                                                {row.title ? row.title : row.story_title && row.story_title}
                                            </TitleDateTableCell>
                                            <AuthorTableCell component="th" id={row.author} scope="row" padding="none">
                                                {row.author && row.author}
                                            </AuthorTableCell>
                                            <TitleDateTableCell component="th" id={row.created_at} scope="row" padding="none">
                                                {row.created_at && formatDate(row.created_at)}
                                            </TitleDateTableCell>
                                            <TableCell align="left">
                                                <ActionMenu
                                                    remove={(e) => handleRemovalDialog(row._id, row.title ? row.title : row.story_title)}
                                                    close={handleMenuClose} />
                                            </TableCell>
                                        </PostTableRow>
                                    );
                                })}
                            {emptyRows > 0 && (
                                <TableRow style={{ height: 33 * emptyRows }}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </div>
    );
}

export default EnhancedTable