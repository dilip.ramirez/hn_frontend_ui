import React from 'react'
import {
    IconButton
} from '@material-ui/core'
import {
    Delete as RemoveIcon
} from '@material-ui/icons'

const ActionMenu = ({ id, remove }) => (
    <>
        <IconButton
            onClick={(e) => {
                e.stopPropagation()
                remove(id)
            }}
        >
            <RemoveIcon />
        </IconButton>
    </>
)

export default ActionMenu