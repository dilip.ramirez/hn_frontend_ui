import React from "react";
import {
    Grid
} from '@material-ui/core'
import EnhancedTable from './EnhancedTable'

function PostsTable() {
    return (
        <>
            <Grid container spacing={6}>
                <Grid item xs={12}>
                    <EnhancedTable />
                </Grid>
            </Grid>
        </>
    );
}

export default PostsTable;
