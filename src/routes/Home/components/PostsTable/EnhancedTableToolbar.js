import React from 'react'
import styled from 'styled-components'
import {
    IconButton,
    Toolbar,
    Typography,
    Tooltip,

} from '@material-ui/core'

import {
    Delete as RemoveIcon,
    FilterList as FilterListIcon,
} from '@material-ui/icons'

const Spacer = styled.div`
  flex: 1 1 100%;
`;

function EnhancedTableToolbar(props) {
    const { numSelected } = props

    return (
        <Toolbar>
            <div>
                {numSelected > 0 ? (
                    <Typography color="inherit" variant="subtitle1">
                        {numSelected} selected
                    </Typography>
                ) : (
                        <Typography variant="h6" id="tableTitle">
                            Posts
                        </Typography>
                    )}
            </div>
            <Spacer />
            <div>
                {numSelected > 0 ? (
                    <Tooltip title="Delete">
                        <IconButton aria-label="Delete">
                            <RemoveIcon />
                        </IconButton>
                    </Tooltip>
                ) : (
                        <Tooltip title="Filter list">
                            <IconButton aria-label="Filter list">
                                <FilterListIcon />
                            </IconButton>
                        </Tooltip>
                    )}
            </div>
        </Toolbar>
    )
}

export default EnhancedTableToolbar