import Home from './Home'

const HomeRoutes = {
    id: 'Home',
    children: [
        {
            ...Home
        }
    ]
}

export const home = [HomeRoutes]