import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";

import { createBrowserHistory } from 'history'

import {
  home as homeRoutes,
} from "./index";

import Public from 'layouts/Public'

const history = createBrowserHistory()

const renderRoute = (index, path, Layout, Component) => (
  <Route
    key={index}
    path={path}
    exact
    render={props => (
      <Layout {...props}>
        <Component {...props} />
      </Layout>
    )}
  />
)
const childRoutes = (Layout, routes) =>
  routes.map(({ children, path, component: Component }, index) => {
    return children ? (
      // Route item with children
      children.map(({ path, component: Component }, index) => renderRoute(index, path, Layout, Component))
    ) : renderRoute(index, path, Layout, Component)
  })

const Routes = () => (
  <Router history={history}>
    <Switch>
      {childRoutes(Public, homeRoutes)}
      
    </Switch>
  </Router>
);

export default Routes;
